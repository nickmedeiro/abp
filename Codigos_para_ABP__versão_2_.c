#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct ABP{

    int chave;
    struct ABP *esq;
    struct ABP *dir;

}ABP;

ABP *inserir (ABP **raiz, int k);
ABP *pesquisar (ABP *raiz, int k);
ABP *encontraPai (ABP *raiz, int k);
ABP **BuscaPontPont (ABP **raiz, int k);
ABP **BuscaMaior (ABP **raiz);
ABP **BuscaMenor (ABP **raiz);
void TrocaDados (ABP *raiz1, ABP *raiz2);	//procedimento para trocar os dados de um n�, neste caso trocando o valor chave
void removerV1 (ABP **raiz, int k);		//remo��o com 1 filho
void removerV2 (ABP **raiz, int k);		//remo��o com 2 filhos
void removerV3 (ABP **raiz, int k);		//remo��o com 3 filhos

ABP *inserir (ABP **raiz, int k){

    assert(raiz);

    if ((*raiz) == NULL){

        (*raiz) = (ABP*) malloc(sizeof(ABP));
        if (!(*raiz)) return NULL;

        (*raiz)->chave = k;
        (*raiz)->dir = (*raiz)->esq = NULL;
        return *raiz;

    }

    if (k < (*raiz)->chave)
        return inserir(&((*raiz)->esq), k);
    

    if (k > (*raiz)->chave)
        return inserir(&((*raiz)->dir), k);
    

 }

ABP *pesquisar (ABP *raiz, int k){

	assert(raiz);

	if (raiz->chave == k) return raiz;

	if (k < raiz->chave) return pesquisar (raiz->esq, k);
	else if (k > raiz->chave) return pesquisar (raiz->dir, k);
	else return NULL;

}

/*
*   Com base em ABP *pesquisar (ABP *raiz, int k), fa�a uma fun��o para encontrar o n� que deve ser inserido e retornar o CAMPO
*   (esq ou dir) que deve ser inserido
*/

ABP *encontraPai(ABP *raiz, int k){

    assert(raiz);

    if (raiz == NULL)
        return raiz;
    
	if (k < raiz->chave)
        return encontraPai(raiz->esq, k);
        
    if (k > raiz->chave)
        return encontraPai(raiz->dir, k);
    
}

/*
*	Fun��o util em procedimento de remo��o, pois retorna o ponteiro de ponteiro do n� que se deseja remover
*/

ABP **BuscaPontPont (ABP **raiz, int k){
	
	assert(raiz);
	
	if ((*raiz)->chave == k) 
		return raiz;
	
	if (k < (*raiz)->chave) 
		return BuscaPontPont (&((*raiz)->esq), k);
	if (k > (*raiz)->chave) 
		return BuscaPontPont (&((*raiz)->dir), k);
	
}

ABP **BuscaMaior (ABP **raiz){

	assert(raiz);

	if((*raiz)->dir == NULL) return raiz;	
	else return BuscaMaior(&((*raiz)->dir));

}

ABP **BuscaMenor (ABP **raiz){

	assert(raiz);

	if((*raiz)->esq == NULL) return raiz;	
	else return BuscaMenor(&((*raiz)->esq));

}

void removerV1 (ABP **raiz, int k){
	
	assert(raiz);
	
	ABP **pp = BuscaPontPont(&(*raiz), k);
	
	if((*pp)->esq == NULL && (*pp)->dir == NULL){
		
		free(*pp);
		(*pp) = NULL;
		return; 
		
	}
	
}

void removerV2 (ABP **raiz, int k){
	
	assert (raiz);
	
	ABP **pp = BuscaPontPont(&(*raiz), k);
	ABP *filho = NULL;
	
	if (((*pp)->esq == NULL) != ((*pp)->dir == NULL)){
		
		filho = ((*pp)->esq == NULL) ? (*pp)->dir : (*pp)->esq;
		free(*pp);
		*pp=filho;
		return;
		
	}
	
}

void removerV3 (ABP **raiz, int k){
	
	assert (raiz);
	
	ABP **pp = BuscaPontPont (&(*raiz), k);
	
	if ((*pp)->esq && (*pp)->dir){
		
		ABP **subs = BuscaMenor(&((*raiz)->dir));
		TrocaDados (*pp, *subs);
		removerV1 (&(*subs), ((*subs)->chave));	//pois o subs ter� somente um filho
		
	}
}

void TrocaDados (ABP *raiz1, ABP *raiz2){
	
	assert (raiz1 && raiz2);
	ABP *aux = NULL;
	
	aux->chave = raiz1->chave;
	raiz1->chave = raiz2->chave;
	raiz2->chave = aux->chave;
		
}

int main(){

    ABP *raiz = NULL;

    inserir (&raiz, 3); //nem sempre � necess�rio armazenar o end aonde � salvo o n�, ling C permite n�o salvarmos o retorno de uma fun��o, se assim o quisermos
    inserir (&raiz, 7);
    
    ABP **pp = BuscaPontPont(&raiz,7);
	ABP **buscaMenorDosMaiores = BuscaMenor(&(raiz->dir)); //vai para o lado dir da arvore, para buscar o menor dentre esses elementos da subarv dir
	ABP **buscaMaiorDosMenores = BuscaMaior(&(raiz->esq)); //vai para o lado esq da arvore, para buscar o maior dentre esses elementos da subarv esq
	
    return 0;

}
