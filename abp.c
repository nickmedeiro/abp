#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct ABP{

    int chave;
    struct ABP *dir;
    struct ABP *esq;

}ABP;


ABP *insere(ABP **raiz, int chave){

    assert(&raiz);

    if((*raiz)->chave == NULL){

        (*raiz) = (ABP*) malloc (sizeof(ABP));
        (*raiz)->chave = chave;
        (*raiz)->dir = (*raiz)->esq = NULL;
        return *raiz;
    }

    if(chave < ((*raiz)->chave)){
        return insere(&((*raiz)->esq), chave);
    }

    if(chave > ((*raiz)->chave)){
        return insere(&((*raiz)->dir), chave);
    }

}

ABP *busca (ABP *raiz, int chave){

    assert (raiz);

    if(raiz -> chave == chave) return raiz;

    if(chave < (raiz->chave)) return busca((raiz->esq), chave);

    if(chave > (raiz->chave)) return busca((raiz->dir), chave);
}

int main(){


return 0;
}
